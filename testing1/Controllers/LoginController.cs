﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;
using testing1.models;

namespace testing1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Base.Contex
    {
        public LoginController(IHostingEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("VerifyLoginDetail")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public login VerifyLoginDetail([FromBody] login data)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_ticketConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    DataTable dt = new DataTable();
                     if(cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }
                    cmd.Connection = cn;
                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@password", data.password));

                    cmd.CommandText = $"SELECT * FROM {this.TicketDatabase}.dbo.Users WHERE username = @username AND password = @password";

                    using(SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if(dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            data.name = row["Name"].ToString();
                            data.surname = row["Surname"].ToString();
                            data.id = Convert.ToInt32(row["clientId"].ToString());
                            data.email = row["Email"].ToString();
                            data.type = row["UserType"].ToString();
                            data.loggedIn = true;
                        }
                    }
                    else
                    {
                        data.loggedIn = false;
                    }




                }

            }
            catch (System.Exception e)
            {

                var msg = e.Message;
            }
            return data;
        }

        [HttpGet]
        [Route("TestConnection")]
        public Connection TestingConnection()
        {
            Connection cn = new Connection();
            try
            {
                using(SqlConnection cc = new SqlConnection(_ticketConnectionString))
                {
                    if(cc.State != ConnectionState.Open)
                    {
                        cc.Open();
                        cn.valid = true;
                    }
                }
            }
            catch (Exception e)
            {
                cn.valid = false;
                var msg = e.Message;
            }

            return cn;
        }

        [HttpPost]
        [Route("ConfirmPasswordOld")]
       //[Authorize(Roles ="user")]
        public Connection ConfirmPasswordOld([FromBody] Password data)
        {
            Connection cn = new Connection();
            try
            {
                using(SqlConnection cc = new SqlConnection(_ticketConnectionString))
                {
                    if(cc.State != ConnectionState.Open)
                    {
                        cc.Open();
                    }
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cc;

                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@password", data.oldPassword));

                    cmd.CommandText = $"SELECT * FROM {TicketDatabase}.dbo.Users WHERE Username = @username AND Password = @password";

                    using(SqlDataReader dr = cmd.ExecuteReader())
                    {
                        dt.Load(dr);
                    }

                    if(dt.Rows.Count > 0)
                    {
                        cn.valid = true;
                    }
                    else
                    {
                        cn.valid = false;
                    }

                }
            }
            catch (Exception e)
            {

                var msg = e.Message;
            }
            return cn;
        }

        [HttpPost]
        [Route("UpdatePassword")]
     //[Authorize(Roles ="user")]
        public Connection UpdatePassword([FromBody] Password data)
        {
            Connection cn = new Connection();
            try
            {
                using(SqlConnection cc =new SqlConnection(_ticketConnectionString))
                {
                    if(cc.State != ConnectionState.Open)
                    {
                        cc.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cc;

                    cmd.Parameters.Add(new SqlParameter("@password", data.newPassword));
                    cmd.Parameters.Add(new SqlParameter("@username", data.username));
                    cmd.Parameters.Add(new SqlParameter("@oldpassword", data.oldPassword));

                    cmd.CommandText = $"UPDATE {TicketDatabase}.dbo.Users SET Password = @password, ChangeDate = GETDATE()  WHERE Username = @username AND Password = @oldpassword";
                    cmd.ExecuteNonQuery();
                    cn.valid = true;
                }
            }
            catch (Exception e)
            {

                var msg = e.Message;
            }


            return cn;
        }

    }
}