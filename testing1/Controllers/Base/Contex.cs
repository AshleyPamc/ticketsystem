﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace testing1.Controllers.Base
{
    public class Contex : ControllerBase
    {
        public static IHostingEnvironment _env;
        public IConfiguration _configuration;
        public static string _ticketConnectionString;
        public static string _EmailWebSvcConnection;
        public static string _EmailServerAddress;
        public static string _EmailUsername;
        public static string _EmailPassword;
        public static string _threadid;
        public static int _port;
        public static string _newid;
        internal SqlConnection _ticketPortalConnection = null;
        public Contex(IHostingEnvironment env, IConfiguration con)
        {
            _env = env;
            _configuration = con;
            var connections = _configuration.GetSection("ConnectionStrings").GetChildren().AsEnumerable();
            var webCon = _configuration.GetSection("WebConnections").GetChildren().AsEnumerable();
            var emailInfo = _configuration.GetSection("EmailServerInfo").GetChildren().AsEnumerable();
            _EmailServerAddress = emailInfo.ToArray()[2].Value;
            _EmailPassword = emailInfo.ToArray()[0].Value;
            _port = Convert.ToInt32(emailInfo.ToArray()[1].Value);
            _EmailUsername = emailInfo.ToArray()[3].Value;
            _ticketConnectionString = connections.ToArray()[0].Value;
            _EmailWebSvcConnection = webCon.ToArray()[0].Value;
            this._ticketPortalConnection = new SqlConnection(_ticketConnectionString);
        }

        public string TicketDatabase
        {
            get
            {
                return this._ticketPortalConnection.Database;
            }
        }
    }
}

