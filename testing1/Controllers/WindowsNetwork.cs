﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Principal;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Win32.SafeHandles;

namespace Sugoi.Utils.WindowsNetwork
{
  public static class NetworkLogon
  {
    [DllImport("advapi32.dll", SetLastError = true)]
    private static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, out SafeAccessTokenHandle phToken);


        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    private unsafe static extern int FormatMessage(int dwFlags, ref IntPtr lpSource, int dwMessageId, int dwLanguageId, ref String lpBuffer, int nSize, IntPtr* arguments);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern bool CloseHandle(IntPtr handle);

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public extern static bool DuplicateToken(IntPtr existingTokenHandle, int SECURITY_IMPERSONATION_LEVEL, ref IntPtr duplicateTokenHandle);


    // logon types
    const int LOGON32_LOGON_INTERACTIVE = 2;
    const int LOGON32_LOGON_NETWORK = 3;
    const int LOGON32_LOGON_NEW_CREDENTIALS = 9;

    // logon providers
    const int LOGON32_PROVIDER_DEFAULT = 0;
    const int LOGON32_PROVIDER_WINNT50 = 3;
    const int LOGON32_PROVIDER_WINNT40 = 2;
    const int LOGON32_PROVIDER_WINNT35 = 1;

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    //public static FileInfo[] GetFilesInPath(string path, string user, string domain, string password, FileInfo[] files)
    //{
    //  System.Collections.ArrayList temp = new System.Collections.ArrayList();

    //  foreach (FileInfo f in files)
    //  {
    //    temp.Add(f);
    //  }

    //  bool isSuccess;
    //  WindowsIdentity newIdentity;
    //  GetNewIdentity(user, domain, password, out isSuccess, out newIdentity);
    //  //FileInfo[] files = null;
    //  using (newIdentity.Impersonate())
    //  {
    //    DirectoryInfo dirInfo = new DirectoryInfo(path);
    //    files = dirInfo.GetFiles();
    //    foreach (FileInfo f in files)
    //    {
    //      temp.Add(f);
    //    }
    //    DirectoryInfo[] ds = dirInfo.GetDirectories();
    //    foreach (DirectoryInfo d in ds)
    //    {
    //      files = new FileInfo[0];
    //      files = GetFilesInPath(d.FullName, user, domain, password, files);
    //      foreach (FileInfo f in files)
    //      {
    //        temp.Add(f);
    //      }
    //    }
    //  }
    //  newIdentity.Dispose();

    //  FileInfo[] completeList = (FileInfo[])(temp.ToArray(typeof(FileInfo)));
    //  //foreach (System.Collections.ArrayList ar in temp)
    //  //{
    //  //    int cnt = ar.Count;
    //  //    completeList[cnt] = ar[cnt];
    //  //}
    //  //fill completeList with temp

    //  return completeList;
    //}

    private static bool GetNewIdentity(string user, string domain, string password, out bool isSuccess, out WindowsIdentity newIdentity)
    {
      IntPtr token = IntPtr.Zero;
      IntPtr dupToken = IntPtr.Zero;

      isSuccess = LogonUser(user, domain, password, LOGON32_LOGON_NEW_CREDENTIALS, LOGON32_PROVIDER_DEFAULT, ref token);
      if (!isSuccess)
      {
        RaiseLastError();
      }

      isSuccess = DuplicateToken(token, 2, ref dupToken);
      if (!isSuccess)
      {
        RaiseLastError();
      }

      newIdentity = new WindowsIdentity(dupToken);

      return isSuccess;
    }

    //public static bool DeleteFile(string folder, string fileToDel, string user, string host, string password)
    //{
    //  bool isSuccess;
    //  WindowsIdentity newIdentity;
    //  GetNewIdentity(user, host, password, out isSuccess, out newIdentity);
    //  using (newIdentity.Impersonate())
    //  {
    //    FileInfo file = new FileInfo(folder + fileToDel);
    //    file.Delete();
    //  }
    //  return isSuccess;
    //}

    public static bool CopyFile(string saveToDirectory, string fileToCopy, string user, string domain, string password)
    {
      

      
      bool isSuccess = false;
      try
      {
        if (new Uri(saveToDirectory).IsUnc)
        {

                    SafeAccessTokenHandle safeAccessTokenHandle;
                    bool returnValue = LogonUser(user, domain, password,
                        LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                        out safeAccessTokenHandle);

                    WindowsIdentity newIdentity;
          GetNewIdentity(user, domain, password, out isSuccess, out newIdentity);
                    WindowsIdentity.RunImpersonated(safeAccessTokenHandle, () =>
                    {
                        FileInfo file = new FileInfo(fileToCopy);
                        string newFilename = saveToDirectory + file.Name;
                        FileInfo newFile = new FileInfo(newFilename);
                        while (newFile.Exists)
                        {
                            int ind = newFile.Name.IndexOf(newFile.Extension);
                            string newName = newFile.Name.Substring(0, ind) + "_1";
                            newName = saveToDirectory + newName + newFile.Extension;
                            newFile = new FileInfo(newName);
                        }
                        file.CopyTo(newFile.FullName);
                       // fileRet = newFile.FullName;
                        if (isSuccess)
                        {
                            file.Delete();
                        }
                    });

        }
        else
        {
          FileInfo file = new FileInfo(fileToCopy);
          string newFilename = saveToDirectory + file.Name;
          FileInfo newFile = new FileInfo(newFilename);
          while (newFile.Exists)
          {
            int ind = newFile.Name.IndexOf(newFile.Extension);
            string newName = newFile.Name.Substring(0, ind) + "_1";
            newName = saveToDirectory + newName + newFile.Extension;
            newFile = new FileInfo(newName);
          }
          file.CopyTo(newFile.FullName);
          newFile = new FileInfo(newFilename);
         // fileRet = newFile.FullName;
          if (newFile.Exists)
          {
            isSuccess = true;
          }
          if (isSuccess)
          {
            file.Delete();
          }
        }
      }
      catch (Exception ex)
      {
        //fileRet = "";
        isSuccess = false;
        Console.WriteLine($"CopyFile error.\r\n  Directory: {saveToDirectory}\r\n  User: {user}\r\n Host : {domain}\r\n Password: {password} " +
            $"\r\nMessage: {ex.Message}\r\nStackT: {ex.StackTrace}");

                FileInfo f = new FileInfo(fileToCopy);
                DirectoryInfo  dr = f.Directory;
                
                FileInfo txt = new FileInfo(dr.FullName + $"\\{DateTime.Now.ToString("yyyyMMddTHHmmss")}_WindNetError.txt");
                if (txt.Exists)
                {
                    StreamWriter sw = new StreamWriter(txt.FullName);
                    sw.WriteLine(ex.Message);
                    sw.WriteLine(ex.StackTrace);
                    sw.Close();
                }
                else
                {
                    
                    StreamWriter sw = new StreamWriter(txt.FullName);
                    sw.WriteLine(ex.Message);
                    sw.WriteLine(ex.StackTrace);
                    sw.Close();

                }

                throw;
      }
      return isSuccess;
    }

    //public static bool MoveFile(string saveFromFile, string saveToFile, string user, string host, string password)
    //{
    //  bool isSuccess;
    //  WindowsIdentity newIdentity;
    //  GetNewIdentity(user, host, password, out isSuccess, out newIdentity);
    //  using (newIdentity.Impersonate())
    //  {
    //    FileInfo fi = new FileInfo(saveFromFile);
    //    if (fi.Exists)
    //    {
    //      fi.CopyTo(saveToFile);
    //      fi.Delete();
    //    }
    //  }
    //  return isSuccess;
    //}

    //public static string UnzipFile(string fileToUnzip, string saveToDirectory, string user, string host, string password)
    //{
    //  bool isSuccess;
    //  WindowsIdentity newIdentity;
    //  GetNewIdentity(user, host, password, out isSuccess, out newIdentity);

    //  string unzippedName = "";
    //  using (newIdentity.Impersonate())
    //  {
    //    ZipInputStream s = new ZipInputStream(File.OpenRead(fileToUnzip));

    //    ZipEntry theEntry;
    //    string tmpEntry = String.Empty;
    //    while ((theEntry = s.GetNextEntry()) != null)
    //    {
    //      string directoryName = saveToDirectory;
    //      unzippedName = Path.GetFileName(theEntry.Name);
    //      // create directory 
    //      if (directoryName != "")
    //      {
    //        Directory.CreateDirectory(directoryName);
    //      }
    //      if (unzippedName != String.Empty)
    //      {
    //        if (theEntry.Name.IndexOf(".ini") < 0)
    //        {
    //          string fullPath = directoryName + "\\" + theEntry.Name;
    //          fullPath = fullPath.Replace("\\ ", "\\");
    //          string fullDirPath = Path.GetDirectoryName(fullPath);
    //          if (!Directory.Exists(fullDirPath)) Directory.CreateDirectory(fullDirPath);
    //          FileStream streamWriter = File.Create(fullPath);
    //          int size = 2048;
    //          byte[] data = new byte[2048];
    //          while (true)
    //          {
    //            size = s.Read(data, 0, data.Length);
    //            if (size > 0)
    //            {
    //              streamWriter.Write(data, 0, size);
    //            }
    //            else
    //            {
    //              break;
    //            }
    //          }
    //          streamWriter.Close();
    //        }
    //      }
    //    }
    //    s.Close();

    //  }
    //  return unzippedName;
    //}

    // GetErrorMessage formats and returns an error message
    // corresponding to the input errorCode.
    public unsafe static string GetErrorMessage(int errorCode)
    {
      int FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
      int FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
      int FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;

      int messageSize = 255;
      string lpMsgBuf = "";
      int dwFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS;

      IntPtr ptrlpSource = IntPtr.Zero;
      IntPtr ptrArguments = IntPtr.Zero;

      int retVal = FormatMessage(dwFlags, ref ptrlpSource, errorCode, 0, ref lpMsgBuf, messageSize, &ptrArguments);
      if (retVal == 0)
      {
        throw new ApplicationException(string.Format("Failed toformat message for error code '{0}'.", errorCode));
      }

      return lpMsgBuf;
    }

    private static void RaiseLastError()
    {
      int errorCode = Marshal.GetLastWin32Error();
      string errorMessage = GetErrorMessage(errorCode);

      throw new ApplicationException(errorMessage);
    }
  }

}
