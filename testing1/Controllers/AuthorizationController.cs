﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using testing1.models;

namespace testing1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : Base.Contex
    {
        private string fileName = "app.txt";
        private IHostingEnvironment _env;

        public AuthorizationController(IHostingEnvironment env, IConfiguration con) : base(env,con)
        {
            _env = env;
        }

        [HttpPost]
        [Route("token")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ActionResult GetToken([FromBody] login data)
        {
            var token = new JwtSecurityToken();
            var root = _env.WebRootPath;
            var roles = new List<Claim>();
            //  roles.Add(new Claim(ClaimTypes.Role, "Administrator"));
            // roles.Add(new Claim(ClaimTypes.Role, "NormalUser"));
            DirectoryInfo di = new DirectoryInfo($"{root}\\tmp");
            FileInfo f = new FileInfo($"{root}\\tmp\\{ fileName }");
            string key;


            //security key
            using (StreamReader reader = new StreamReader(f.FullName))
            {
                key = reader.ReadLine();
                reader.Close();
            }

            //symetric security key


            var symetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            //signing credentials

            var signingCredentials = new SigningCredentials(symetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            //Adding Roles


            //create token


                    roles.Add(new Claim(ClaimTypes.Role, "user"));
                    token = new JwtSecurityToken(
                    issuer: "ticketportal.in",
                    audience: "authorizedUsers",
                    expires: DateTime.Now.AddHours(8),
                    signingCredentials: signingCredentials,
                    claims: roles
                    );




            //return token
            return Ok(new JwtSecurityTokenHandler().WriteToken(token));

        }
   

        
    
    }

}
