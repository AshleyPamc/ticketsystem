﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testing1.models
{
    public class Tickets
    {
        public string changeDate;
        public string changeby;
        public string createDate;
        public string user;
        public string name;
        public string surname;
        public string status;
        public string threadId;
        public string assignedTo;
        public string assignedToEmail;
        public string email;
        public string subject;
        public string message;
        public string reference;
        public string category;
        public string importance;
        public string aknowledge;
        public string completedTime;
        public string ticketId;
        public string clientIdDesc;
        public string clientId;
        public bool hasDocuments;
        public string catid;
        public string statDisc;
        public string time ;
        public string  comment;
        public bool statusChange;
        public bool assignedChange;
        public bool importanceChange;
        public bool categoryChange;
        public bool commentChange;
        public string cancelReason;
    }
}
