﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testing1.models
{
    public class Users
    {
        public string client;
        public string name;
        public string surname;
        public string username;
        public string email;
        public string createDate;
        public string changeDate;
        public string type;
        public string password;
        public string id;
    }
}
