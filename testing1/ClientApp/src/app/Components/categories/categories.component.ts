import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Categories } from '../../models/categories';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  public _openModel: boolean = false;

  @Input() catForm: FormGroup;

  constructor(public _appService: AppService, public fb: FormBuilder) {
    this._appService.AdminService.GetCategories();
  }

  ngOnInit() {

    this.catForm = this.fb.group({
      'desc': ['', Validators.required],
      'sla': ['', Validators.required],
    });
  }

  CreateNew() {
    this.catForm.reset();
    this._openModel = true;
  }

  AddNewCategory() {
    let c: Categories = new Categories();

    c.desc = this.catForm.get('desc').value;
    c.sla = this.catForm.get('sla').value;

    this._appService.AdminService.AddCategory(c);

    this._openModel = false;
    this.catForm.reset();
  }
}
