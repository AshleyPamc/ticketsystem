import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Users } from '../../models/users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  public _openModel: boolean = false;
  public _edit: boolean = false;
  public _username: string;

  @Input() userForm: FormGroup;

  constructor(public _appService: AppService, private fb: FormBuilder) {
    this._appService.AdminService.GetUsers();
    this._appService.AdminService.GetClients();
   
  }

  ngOnInit() {

    this.userForm = this.fb.group({
      'name': ['', Validators.required],
      'surname': ['', Validators.required],
      'client': ['', Validators.required],
      'email': ['', Validators.required],
      'password': ['', Validators.required],
      'type': ['', Validators.required],
      'username': ['', Validators.required]
    });

  }

  OpenEdit(data:Users) {
    this._edit = true;
    this.userForm.reset();
    this._username = data.username;

    this.userForm.controls['name'].setValue(data.name);
    this.userForm.controls['surname'].setValue(data.surname);
    this.userForm.controls['email'].setValue(data.email);
    this.userForm.controls['password'].setValue(data.password);
    this._appService.AdminService.clients.forEach((el) => {
      if (el.name == data.client) {
        this.userForm.controls['client'].setValue(el.id);
      }
    })
    let c: String = data.type.trimRight(); 
    switch (c) {
      case "Admin":
        this.userForm.controls['type'].setValue("0");
        break;
      case "SysAdmin":
        this.userForm.controls['type'].setValue("2");
        break;
      case "Local":
        this.userForm.controls['type'].setValue("1");
        break;
      case "LocalAdmin":
        this.userForm.controls['type'].setValue("3");
        break;
      default:
        break;
    }

    this._openModel = true;
  }

  OpenNew() {
    this._edit = false;
    this._openModel = true;
    this.userForm.reset();
  }

  updateUser() {
    let c: Users = new Users();

    c.name = this.userForm.get('name').value;
    c.surname = this.userForm.get('surname').value;
    c.client = this.userForm.get('client').value;
    c.email = this.userForm.get('email').value;
    c.password = this.userForm.get('password').value;
    c.username = this._username;
    c.type = this.userForm.get('type').value;

    if ((c.type == null) || (c.type == undefined) || (c.type == "")) {
      this._appService.AdminService.users.forEach((el) => {
        if (el.username == c.username) {
          let d: String = el.type.trimRight();
          switch (d) {
            case "Admin":
              c.type = "0";
              break;
            case "SysAdmin":
              c.type = "2";
              break;
            case "Local":
              c.type = "1";
              break;
            case "LocalAdmin":
              c.type = "3";
              break;
            default:
              break;
          } 
        }
      })
    }

    this._appService.AdminService.UpdateUser(c);
    this.userForm.reset();
    this._openModel = false;
  }

  AddNewUser() {
    this._edit = false;
    let c: Users = new Users();

    c.name = this.userForm.get('name').value;
    c.surname = this.userForm.get('surname').value;
    c.username = this.userForm.get('username').value;
    c.client = this.userForm.get('client').value;
    c.email = this.userForm.get('email').value;
    c.password = this.userForm.get('password').value;
   // c.username = this._username;
    c.type = this.userForm.get('type').value;

    this._appService.AdminService.AddUser(c);
    this.userForm.reset();
    this._openModel = false;
  }

}
