import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Tickets } from '../../models/tickets';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  @Input() searchForm: FormGroup;

  constructor(public _appService: AppService, private searchFb: FormBuilder) {
    this._appService.TicketService.GetAllTickets();
  }

  ngOnInit() {
    this.searchForm = this.searchFb.group({
      'search': []
    });
  }

  Filter() {
    let temp: Tickets[] = [];
    let c: string = this.searchForm.get('search').value;

    this._appService.TicketService.busy = true;

    this._appService.TicketService.listOfAllTickets.forEach((el) => {
      if (el.reference.includes(c)) {
        temp.push(el);
      }
      if (el.subject.includes(c)) {
        temp.push(el);
      }
      if (el.name.includes(c)) {
        temp.push(el);
      }
      if (el.surname.includes(c)) {
        temp.push(el);
      }
      if (el.email.includes(c)) {
        temp.push(el);
      }
      if (el.createDate.includes(c)) {
        temp.push(el);
      }
      if (el.category.includes(c)) {
        temp.push(el);
      }
    });

    this._appService.TicketService.listOfAllTickets = [];
    temp.forEach((e) => {
      this._appService.TicketService.listOfAllTickets.push(e);
    });

    this._appService.TicketService.busy = false;
  }

  ClearFilter() {

    this.searchForm.reset();
    this._appService.TicketService.GetAllTickets();
  }

}
