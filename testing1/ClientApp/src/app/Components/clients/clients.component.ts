import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Clients } from '../../models/clients';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  @Input() clientForm: FormGroup;
  public _openModel: boolean = false;
  public _edit: boolean = false;
  public _id: string;


  constructor(public _appService: AppService, private fb: FormBuilder) {

    this._appService.AdminService.GetClients();
  }

  ngOnInit() {

    this.clientForm = this.fb.group({
      'name': ['', Validators.required],
      'acc': ['' , Validators.required],
      'thresh': ['' , Validators.required]

    })
  }

  CreateNew() {
    this._edit = false;
    this._openModel = true;
  }

  UpdateClient(data: Clients) {
    this._edit = true;
    this._id = data.id;
    this.clientForm.reset();
    this.clientForm.controls['name'].setValue(data.name);
    this.clientForm.controls['acc'].setValue(data.accronym);
    this.clientForm.controls['thresh'].setValue(data.threshold);
    this._openModel = true;
  }

  AddNewClient() {
    let c: Clients = new Clients();

    c.accronym = this.clientForm.get('acc').value.toUpperCase();
    c.name = this.clientForm.get('name').value;
    c.threshold = this.clientForm.get('thresh').value;

    this._appService.AdminService.AddClient(c);
    this.clientForm.reset();
    this._openModel = false;
  }

  UpdateExistClient() {
    let c: Clients = new Clients();

    c.accronym = this.clientForm.get('acc').value.toUpperCase();
    c.name = this.clientForm.get('name').value;
    c.threshold = this.clientForm.get('thresh').value;
    c.id = this._id;
    this._appService.AdminService.UpdateClient(c);
    this.clientForm.reset();
    this._openModel = false;
  }
}
