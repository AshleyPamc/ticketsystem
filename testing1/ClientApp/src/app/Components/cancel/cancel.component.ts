import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../services/app.service';
import { Tickets } from '../../models/tickets';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Login } from '../../models/login';

@Component({
  selector: 'app-cancel',
  templateUrl: './cancel.component.html',
  styleUrls: ['./cancel.component.css']
})
export class CancelComponent implements OnInit {

  private _selectedSubject: string;
  private _selectMessage: string;
  private _selectedFrom: string;
  private _openDetailModel: boolean = false;
  private _openUpdate: boolean = false;

  private _hasDocs: boolean = false;
  private _selectedTicket: Tickets = new Tickets();
  public _selectedTicketLIst: Tickets[] = [];
  private _openReply: boolean = false;
  public files: File[] = [];

  @Input() usersForm: FormGroup;
  @Input() msgForm: FormGroup;
  @Input() DetailsForm: FormGroup;
  @Input() searchForm: FormGroup;

  constructor(public _appService: AppService, private fb: FormBuilder, private _toastService: ToastrService, private searchFb: FormBuilder,private msgFb: FormBuilder, private detFb: FormBuilder, private _datePipe: DatePipe) {

    this._appService.TicketService._loginDetails = new Login();
    this._appService.TicketService._loginDetails = this._appService.LoginService.logindetails;
    this._appService.TicketService.GetAllTickets();
  }

  ngOnInit() {

    this.usersForm = this.fb.group({
      'names': [],
      'users': []
    });

    this.msgForm = this.msgFb.group({
      'msg': ['', Validators.required],
      'subject': ['', Validators.required]
    })

    this.DetailsForm = this.detFb.group({
      'users': ['', Validators.required],
      'comments': ['', Validators.required],
      'duration': ['', Validators.required],
      'status': ['', Validators.required],
      'category': ['', Validators.required],
      'importance': ['', Validators.required],
      'msg': ['', Validators.required]
    });
    if (this._appService.LoginService.logindetails.type == '3') {
      this.DetailsForm.disable();
    }
    this.searchForm = this.searchFb.group({
      'search': []
    })

  }


  public get openReply(): boolean {
    return this._openReply;
  }
  public set openReply(value: boolean) {
    this._openReply = value;
  }

  public get selectedSubject(): string {
    return this._selectedSubject;
  }
  public set selectedSubject(value: string) {
    this._selectedSubject = value;
  }
  public get selectMessage(): string {
    return this._selectMessage;
  }
  public set selectMessage(value: string) {
    this._selectMessage = value;
  }
  public get selectedFrom(): string {
    return this._selectedFrom;
  }
  public set selectedFrom(value: string) {
    this._selectedFrom = value;
  }
  public get openDetailModel(): boolean {
    return this._openDetailModel;
  }
  public set openDetailModel(value: boolean) {
    this._openDetailModel = value;
  }
  public get hasDocs(): boolean {
    return this._hasDocs;
  }
  public set hasDocs(value: boolean) {
    this._hasDocs = value;
  }
  public set selectedTicket(value: Tickets) {
    this._selectedTicket = value;
  }
  public get selectedTicket(): Tickets {
    return this._selectedTicket;
  }
  public get openUpdate(): boolean {
    return this._openUpdate;
  }
  public set openUpdate(value: boolean) {
    this._openUpdate = value;
  }

  OpenTicketDetail(ticket: Tickets) {
    this._selectedTicket = new Tickets();
    this._selectedTicket.time = ticket.time;
    this._selectedTicket.assignedTo = ticket.assignedTo;
    this._selectedTicket.clientIdDesc = ticket.clientIdDesc;
    this._selectedTicket.clientId = ticket.clientId;
    this._selectedTicket.changeDate = ticket.changeDate;
    this._selectedTicket.changeby = ticket.changeby;
    this._selectedTicket.createDate = ticket.createDate;
    this._selectedTicket.surname = ticket.surname;
    this._selectedTicket.category = ticket.category;
    this._selectedTicket.email = ticket.email;
    this._selectedTicket.importance = ticket.importance;
    this._selectedTicket.message = ticket.message;
    this._selectedTicket.name = ticket.name;
    this._selectedTicket.status = ticket.status;
    this._selectedTicket.subject = ticket.subject;
    this._selectedTicket.threadId = ticket.threadId;
    this._selectedTicket.ticketId = ticket.ticketId;
    this._selectedTicket.reference = ticket.reference;
    this._selectedTicket.user = ticket.user;
    this.hasDocs = ticket.hasDocuments;
    this._selectedFrom = ticket.user;
    this._selectedSubject = ticket.subject;
    this._selectMessage = ticket.message;
    this._openDetailModel = true;
    this._appService.TicketService.GetMessages(ticket);
  }

  OpenUpdate(ticket: Tickets) {
    this._appService.TicketService.GetCategories(ticket);
    this._selectedTicket = new Tickets();
    this._selectedTicket.assignedTo = ticket.assignedTo;
    this._selectedTicket.changeDate = ticket.changeDate;
    this._selectedTicket.clientId = ticket.clientId;
    this._selectedTicket.comment = ticket.comment;
    this._selectedTicket.changeby = ticket.changeby;
    this._selectedTicket.createDate = ticket.createDate;
    this._selectedTicket.category = ticket.category;
    this._selectedTicket.clientIdDesc = ticket.clientIdDesc;
    this._selectedTicket.surname = ticket.surname;
    this._selectedTicket.email = ticket.email;
    this._selectedTicket.importance = ticket.importance;
    this._selectedTicket.message = ticket.message;
    this._selectedTicket.name = ticket.name;
    this._selectedTicket.status = ticket.status;
    this._selectedTicket.subject = ticket.subject;
    this._selectedTicket.threadId = ticket.threadId;
    this._selectedTicket.ticketId = ticket.ticketId
    this._selectedTicket.category = ticket.category;
    this._selectedTicket.status = ticket.status;
    this._selectedTicket.importance = ticket.importance;
    this._selectedTicket.reference = ticket.reference;
    this._selectedTicket.cancelReason = ticket.cancelReason;
    this._selectedTicket.user = ticket.user;
    this.hasDocs = ticket.hasDocuments;
    this._selectedFrom = ticket.email;
    this._selectedSubject = ticket.subject;
    this._selectMessage = ticket.message;

    this._openUpdate = true;
    this._selectedTicket.time = ticket.time;
    this.DetailsForm.controls['duration'].setValue(ticket.time);
    //this._appService.TicketService.GetMessages(ticket);

   
    this.DetailsForm.reset();

    this._appService.TicketService._status.forEach((el) => {
      if (el.code == ticket.status) {
        this.DetailsForm.controls['status'].setValue(el.code);
      }
    });

    this._appService.TicketService._categories.forEach((el) => {
      if (el.desc == ticket.category) {
        this.DetailsForm.controls['category'].setValue(el.id);
      }
    });

    this._appService.TicketService._importance.forEach((el) => {
      if (el.desc == ticket.importance) {
        this.DetailsForm.controls['importance'].setValue(el.id);
      }
    });

    this._appService.TicketService._categories.forEach((el) => {
      if (el.desc == ticket.category) {
        this.DetailsForm.controls['category'].setValue(el.id);
      }
    });
    this._appService.TicketService._adminUserList.forEach((el) => {
      if (el.username == ticket.assignedTo) {
        this.DetailsForm.controls['users'].setValue(el.username);
      }
    });
    this.DetailsForm.controls['comments'].setValue(this._selectedTicket.comment);
    this.DetailsForm.controls['msg'].setValue(this._selectedTicket.cancelReason);
  }

  GetSelectedList() {
    let name = this.usersForm.get('names').value;
    this._selectedTicketLIst = [];
    let count = 0;
    let todayDate: Date = new Date();
    let todayDateStr = this._datePipe.transform(todayDate, "yyyy/MM/dd");
    this._appService.TicketService.canceledTickets.forEach(
      (el) => {
        if (el.assignedTo == name) {
          let ticketDate: Date = new Date(el.changeDate);
          let ticketDateStr = this._datePipe.transform(ticketDate, "yyyy/MM/dd");
          this._selectedTicketLIst.push(el);
          if (ticketDateStr == todayDateStr) {
            count++;
          }
        }
      });

    this._toastService.info(name + " has canceled " + count + " ticket/s today.", "CANCELED TICKETS");
  }

  DownloadAttachments() {
    this._appService.TicketService.DownloadSelectedAttachment(this._selectedTicket);
    this.openDetailModel = false;

  }


  OpenReplyBox() {
    this.files = [];
    this.openReply = true;
  }


  upload(event) {
    event.addedFiles.forEach((el) => {
      this.files.push(el);
    });
  }

  SizeCalc(size: number): string {
    return (size / Math.pow(1024, 2)).toFixed(2);
  }

  Remove(name: string, type: string) {
    let count: number = 0;
    let index: number = 0;
    let found: boolean = false;
    this.files.forEach((el) => {
      if ((el.name == name) && (el.type == type)) {
        index = count;
        found = true;
      }
      count++;
    });

    if (found) {
      this.files.splice(index, 1);
    }


  }

  Filter() {
    let temp: Tickets[] = [];
    let c: string = this.searchForm.get('search').value;

    this._appService.TicketService.busy = true;

    this._selectedTicketLIst.forEach((el) => {
      if (el.reference.includes(c)) {
        temp.push(el);
      }
      if (el.subject.includes(c)) {
        temp.push(el);
      }
      if (el.name.includes(c)) {
        temp.push(el);
      }
      if (el.surname.includes(c)) {
        temp.push(el);
      }
      if (el.email.includes(c)) {
        temp.push(el);
      }
      if (el.createDate.includes(c)) {
        temp.push(el);
      }
      if (el.category.includes(c)) {
        temp.push(el);
      }
    });

    this._selectedTicketLIst = [];
    temp.forEach((e) => {
      this._selectedTicketLIst.push(e);
    });

    this._appService.TicketService.busy = false;
  }

  ClearFilter() {

    this.searchForm.reset();
    this.GetSelectedList();
  }

  UploadFiles() {
    let reply: Tickets = new Tickets();
    let p = this;
    reply = p._selectedTicket;
    reply.message = p.msgForm.get('msg').value.replace("'", "");
    reply.reference = p._selectedTicket.reference;
    reply.user = p._appService.LoginService.logindetails.username;
    p._appService.TicketService.files = new FormData;
    p.files.forEach((el) => {
      p._appService.TicketService.files.append(el.name, el);
    });

    p._appService.TicketService.SendReply(reply);

    p.msgForm.reset();
    p.openReply = false;


  }

  UpdateTicket() {
    let c: Tickets = new Tickets();
    c.status = this.DetailsForm.get('status').value;
    if (c.status !== this._selectedTicket.status) {
      c.statusChange = true;
    }
    c.changeby = this._appService.LoginService.logindetails.username;
    c.importance = this.DetailsForm.get('importance').value;
    let imp: string;
    this._appService.TicketService._importance.forEach((el) => {
      if (el.id == c.importance) {
        imp = el.desc;
      }
    })
    if (imp !== this._selectedTicket.importance) {
      c.importanceChange = true;
    }
    let h = this.DetailsForm.get('category').value
    if ((h != undefined) && (h != null) && (h != "")) {
      c.catid = this.DetailsForm.get('category').value;
    } else {
      c.catid = this.selectedTicket.catid;
    }
    if (c.catid !== this._selectedTicket.catid) {
      c.categoryChange = true;
    }


    c.comment = this.DetailsForm.get('comments').value;

    if (c.comment !== this._selectedTicket.comment) {
      c.commentChange = true;
    }

    c.ticketId = this.selectedTicket.ticketId;
    c.time = this.DetailsForm.get('duration').value;
    c.assignedTo = this.DetailsForm.get('users').value;
    if (c.assignedTo !== this._selectedTicket.assignedTo) {
      c.assignedChange = true;
    }

    if (c.status == '4') {
      c.cancelReason = this.DetailsForm.get('msg').value.replace("'", "");
    }

    c.user = this._selectedTicket.user;

    this._appService.TicketService.updateTicket(c);

    this._openUpdate = false;
    this._openReply = false;
  }
}
