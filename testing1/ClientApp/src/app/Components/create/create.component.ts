import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from '../../services/app.service';
import { Tickets } from '../../models/tickets';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  @Input() createForm: FormGroup;
  public files: File[] = [];

  constructor(private fb: FormBuilder, public _appService: AppService) {

    let c: Tickets = new Tickets();
    c.clientId = this._appService.LoginService.logindetails.id.toString();
    

    this._appService.TicketService.GetCategories(c);
    this._appService.TicketService.GetImportanceCodes();
  }

  ngOnInit() {
    this.createForm = this.fb.group({
      'category': ['', Validators.required],
      'importance': ['', Validators.required],
      'subject': ['', Validators.required],
      'msg': ['', Validators.required]
    });

   
  }

  upload(event) {
    event.addedFiles.forEach((el) => {
      this.files.push(el);
    });
  }

  SizeCalc(size: number): string {
    return (size / Math.pow(1024, 2)).toFixed(2);
  }

  Remove(name: string, type: string) {
    let count: number = 0;
    let index: number = 0;
    let found: boolean = false;
    this.files.forEach((el) => {
      if ((el.name == name) && (el.type == type)) {
        index = count;
        found = true;
      }
      count++;
    });

    if (found) {
      this.files.splice(index, 1);
    }


  }

  ClearAll() {
    this.createForm.reset();
    this.files = [];
  }

  SubmitTicket() {
    let c: Tickets = new Tickets();

    c.user = this._appService.LoginService.logindetails.username;
    c.catid = this.createForm.get('category').value;
    c.clientId = this._appService.LoginService.logindetails.id.toString();
    c.message = this.createForm.get('msg').value.replace("'", "");
    c.subject = this.createForm.get('subject').value.replace("'", "");
    c.importance = this.createForm.get('importance').value;
    c.name = this._appService.LoginService.logindetails.name;
    c.surname = this._appService.LoginService.logindetails.surname;
    c.email =  this._appService.LoginService.logindetails.email;

    this._appService.TicketService.files = new FormData;

    this.files.forEach((el) => {
      this._appService.TicketService.files.append(el.name, el);
    });

    this._appService.TicketService.SubmitNewTicket(c);
    this.files = [];
    this.createForm.reset();

  }

}
