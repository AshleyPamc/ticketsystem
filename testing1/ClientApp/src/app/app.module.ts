import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ToastrModule } from 'ngx-toastr';
import { RequestedComponent } from './Components/requested/requested.component';
import { InprocessComponent } from './Components/inprocess/inprocess.component';
import { CancelComponent } from './Components/cancel/cancel.component';
import { DoneComponent } from './Components/done/done.component';
import { AllComponent } from './Components/all/all.component';
import { CreateComponent } from './Components/create/create.component';
import { DatePipe } from '@angular/common';
import {NgxDropzoneModule } from 'ngx-dropzone';
import { ClientsComponent } from './Components/clients/clients.component';
import { UsersComponent } from './Components/users/users.component';
import { CategoriesComponent } from './Components/categories/categories.component';
import { CatAssignComponent } from './Components/cat-assign/cat-assign.component';
import { ImportancComponent } from './Components/importanc/importanc.component';
import { StatusComponent } from './Components/status/status.component';
import { CommentsComponent } from './Components/comments/comments.component';


@NgModule({
  declarations: [
    AppComponent,

    LoginComponent,

    DashboardComponent,

    RequestedComponent,

    InprocessComponent,

    CancelComponent,

    DoneComponent,

    AllComponent,

    CreateComponent,

    ClientsComponent,

    UsersComponent,

    CategoriesComponent,

    CatAssignComponent,

    ImportancComponent,

    StatusComponent,

    CommentsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
      HttpClientModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxDropzoneModule,
    FormsModule,
    
    ClarityModule,
      RouterModule.forRoot([
          { path: 'DashBoard', component: DashboardComponent },
          { path: 'login', component: LoginComponent },
          { path: 'request', component: RequestedComponent },
          { path: 'done', component: DoneComponent },
          { path: 'progress', component: InprocessComponent },
          { path: 'canceled', component: CancelComponent },
        { path: '', component: LoginComponent },
        { path: 'create', component: CreateComponent },
        { path: 'all', component: AllComponent },
        { path: 'clients', component: ClientsComponent },
        { path: 'users', component: UsersComponent },
        { path: 'categories', component: CategoriesComponent },
        { path: 'catAssign', component: CatAssignComponent },
        { path: 'importance', component: ImportancComponent },
        { path: 'status', component: StatusComponent },
        { path: 'comments', component: CommentsComponent }

    ])
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
