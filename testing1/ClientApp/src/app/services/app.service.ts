import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import { TicketService } from './ticket.service';
import { AdminService } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private _loginService: LoginService, private _ticketService: TicketService, private _adminService: AdminService) { }

  public get LoginService(): LoginService {
    return this._loginService;
  }

  public get TicketService(): TicketService {
    return this._ticketService;
  }
  public get AdminService(): AdminService {
    return this._adminService;
  }
}
