import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Login } from '../models/login';
import { Tickets } from '../models/tickets';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { Attachment } from '../models/attachment';
import { Qoutes } from '../models/qoutes';
import { ValidationResultsModel } from '../models/ValidationResultsModel';
import { UserList } from '../models/user-list';
import { Comments } from '../models/comments';
import { Durations } from '../models/durations';
import { Status } from '../models/status';
import { Categories } from '../models/categories';
import { Importance } from '../models/importance';
import { Router } from '@angular/router';
import { Names } from '../models/names';



@Injectable({
  providedIn: 'root'
})
export class TicketService {

  private _busy: boolean = false;
  private _completedTickets: Tickets[] = [];
  private _canceledTickets: Tickets[] = [];
  private _processTickets: Tickets[] = [];
  private _newTickets: Tickets[] = [];
  private _cancledUsers: string[] = [];
  private _inprogressUsers: string[] = [];
  private _doneUsers: string[] = [];
  private _requestedUsers: string[] = [];
  private _selectedUser: Login = new Login();
  public _adminUserList: UserList[] = [];
  public _commentList: Comments[] = [];
  public _durations: Durations[] = [];
  public _importance: Importance[] = [];
  public _categories: Categories[] = [];
  public _status: Status[] = [];
  private _error: boolean = false;
  private _errorMessage: string = "";
  private _selectedThread: Tickets[];
  private _replyTicket: Tickets[] = [];
  private _files: FormData = new FormData();
  public _Success: boolean = false;
  public _sending: boolean = false;
  private _listOfAllTickets: Tickets[] = [];
  public _loginDetails: Login = new Login();
  public newTicketUserCount: Names[] = [];
 









  constructor(private _router: Router,private _http: HttpClient, @Inject('BASE_URL') private _baseUrl: string, private _toastService: ToastrService, private datepipe: DatePipe) {

   // this._Success = false;
  }

  public get listOfAllTickets(): Tickets[] {
    return this._listOfAllTickets;
  }
  public set listOfAllTickets(value: Tickets[]) {
    this._listOfAllTickets = value;
  }
  public get errorMessage(): string {
    return this._errorMessage;
  }
  public set errorMessage(value: string) {
    this._errorMessage = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get completedTickets(): Tickets[] {
    return this._completedTickets;
  }
  public set completedTickets(value: Tickets[]) {
    this._completedTickets = value;
  }
  public get canceledTickets(): Tickets[] {
    return this._canceledTickets;
  }
  public set canceledTickets(value: Tickets[]) {
    this._canceledTickets = value;
  }
  public get processTickets(): Tickets[] {
    return this._processTickets;
  }
  public set processTickets(value: Tickets[]) {
    this._processTickets = value;
  }
  public get newTickets(): Tickets[] {
    return this._newTickets;
  }
  public set newTickets(value: Tickets[]) {
    this._newTickets = value;
  }
  public get cancledUsers(): string[] {
    return this._cancledUsers;
  }
  public set cancledUsers(value: string[]) {
    this._cancledUsers = value;
  }
  public get inprogressUsers(): string[] {
    return this._inprogressUsers;
  }
  public set inprogressUsers(value: string[]) {
    this._inprogressUsers = value;
  }
  public get doneUsers(): string[] {
    return this._doneUsers;
  }
  public set doneUsers(value: string[]) {
    this._doneUsers = value;
  }
  public get requestedUsers(): string[] {
    return this._requestedUsers;
  }
  public set requestedUsers(value: string[]) {
    this._requestedUsers = value;
  }
  public get selectedUser(): Login {
    return this._selectedUser;
  }
  public set selectedUser(value: Login) {
    this._selectedUser = value;
  }
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }
  public get selectedThread(): Tickets[] {
    return this._selectedThread;
  }
  public set selectedThread(value: Tickets[]) {
    this._selectedThread = value;
  }
  public get replyTicket(): Tickets[] {
    return this._replyTicket;
  }
  public set replyTicket(value: Tickets[]) {
    this._replyTicket = value;
  }
  public get files(): FormData {
    return this._files;
  }
  public set files(value: FormData) {
    this._files = value;
  }


  GetAllTickets() {
    //let token = localStorage.getItem('token');
    this.busy = true;
    let count = 0;
    this._http.get(this._baseUrl + 'api/Ticket/GetAllTickets', {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Tickets[]) => {
      results.sort((a, b) => a.status > b.status ? 1 : 0);
      this.doneUsers = [];
      this.inprogressUsers = [];
      this.cancledUsers = [];
      this.requestedUsers = [];
      this.completedTickets = [];
      this.newTickets = [];
      this.canceledTickets = [];
      this.processTickets = [];
      let todayDate: Date = new Date();
      let todayDateStr = this.datepipe.transform(todayDate, "yyyy/MM/dd");

      results.forEach((el) => {
        el.createDate = el.createDate.substr(0, 10);
        if (el.createDate.indexOf("/") == 2) {
          let d = el.createDate.substr(0, 2);
          let m = el.createDate.substr(3, 2);
          let y = el.createDate.substr(6, 4);
          el.createDate = y + "/" + m + "/" + d;
        }
        let ticketDate: Date = new Date(el.createDate);
        let ticketDateStr = this.datepipe.transform(ticketDate, "yyyy/MM/dd");
        if (ticketDateStr == todayDateStr) {
          count++;
        }
        if (el.status == '2') {
          if (!this.doneUsers.includes(el.assignedTo)) {
            this.doneUsers.push(el.assignedTo);

          }
          this.completedTickets.push(el);
        }


        if (el.status == '1') {
          if (!this.requestedUsers.includes(el.assignedTo)) {
            this.requestedUsers.push(el.assignedTo);

          } this.newTickets.push(el);
        }


        if (el.status == '3') {
          if (!this.inprogressUsers.includes(el.assignedTo)) {
            this.inprogressUsers.push(el.assignedTo);

          }
          this.processTickets.push(el);
        }


        if (el.status == '4') {
          if (!this.cancledUsers.includes(el.assignedTo)) {
            this.cancledUsers.push(el.assignedTo);

          }
          this.canceledTickets.push(el);
        }
        this.listOfAllTickets.push(el);
      })

      this.newTicketUserCount = [];
      this._requestedUsers.forEach((el) => {
        let c: Names = new Names();
        let count: number = 0;
        let todayDate: Date = new Date();
        let todayDateStr = this.datepipe.transform(todayDate, "yyyy/MM/dd");
        c.user = el;
        this._newTickets.forEach((e) => {
          if (e.assignedTo == el) {
            let ticketDate: Date = new Date(e.createDate);
            let ticketDateStr = this.datepipe.transform(ticketDate, "yyyy/MM/dd");
            if (ticketDateStr == todayDateStr) {
              count++;
            }
          }
        });
        c.count = count;
        this.newTicketUserCount.push(c);
      })
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => {
        this._busy = false;
        if (this._adminUserList.length == 0) {
          this.GetUserList();
        }
        if (this._commentList.length == 0) {
          this.GetCommentsList();
        }
        if (this._durations.length == 0) {
          this.GetDurations();
        }
        if (this._status.length == 0) {
          this.GetStatus();
        }
        if (this._importance.length == 0) {
          this.GetImportanceCodes();
        }

        if (this._router.url == '/all') {
          this._toastService.success("Sofar " + count + " tickets was logged for today.", "TODAYS TICKETS");
        }

        if (this._loginDetails.type == '3') {
          this.FilterTicket(this._loginDetails.username, '1');
          this.FilterTicket(this._loginDetails.username, '2');
          this.FilterTicket(this._loginDetails.username, '3');
          this.FilterTicket(this._loginDetails.username, '4');
        }

        if (this._loginDetails.type == '3') {
          this.ShowNotification();
        }


      });

  }

  GetUserList() {
    //let token = localStorage.getItem('token');
    this.busy = true;
    this._http.get(this._baseUrl + 'api/Ticket/GetUserList', {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: UserList[]) => {
      this._adminUserList = [];
      results.forEach((el) => {
        this._adminUserList.push(el);
      })
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => { this._busy = false; })
  }

  GetCommentsList() {
    //let token = localStorage.getItem('token');
    this.busy = true;
    this._http.get(this._baseUrl + 'api/Ticket/GetCommentsList', {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Comments[]) => {
      this._commentList = [];
      results.forEach((el) => {
        this._commentList.push(el);
      })
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => { this._busy = false; })
  }


  GetStatus() {
    //let token = localStorage.getItem('token');
    this.busy = true;
    this._http.get(this._baseUrl + 'api/Ticket/GetStatus', {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Status[]) => {
      this._status = [];
      results.forEach((el) => {
        this._status.push(el);
      })
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => { this._busy = false; })
  }

  GetImportanceCodes() {
    //let token = localStorage.getItem('token');
    this.busy = true;
    this._http.get(this._baseUrl + 'api/Ticket/GetImportanceCodes', {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Importance[]) => {
      this._importance = [];
      results.forEach((el) => {
        this._importance.push(el);
      })
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => { this._busy = false; })
  }

  GetCategories(ticket:Tickets) {
    //let token = localStorage.getItem('token');
    this.busy = true;
    this._http.post(this._baseUrl + 'api/Ticket/GetCategories', ticket, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Categories[]) => {
      this._categories = [];
      results.forEach((el) => {
        this._categories.push(el);
      })
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => { this._busy = false; })
  }

  GetDurations() {
    //let token = localStorage.getItem('token');
    this.busy = true;
    this._http.get(this._baseUrl + 'api/Ticket/GetDurations', {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Durations[]) => {
      this._durations = [];
      results.forEach((el) => {
        this._durations.push(el);
      })
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => { this._busy = false; })
  }

  GetMessages(ticket:Tickets) {
    //let token = localStorage.getItem('token');
    this.busy = true;
    this._http.post(this._baseUrl + 'api/Ticket/GetMessages', ticket, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: Tickets[]) => {
      this.selectedThread = [];
      results.forEach((el) => {
        this.selectedThread.push(el);
      });
      this.selectedThread.sort((a, b) => a.createDate > b.createDate ? 1 : -1);
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => {
        this._busy = false;
      })
  }

  UploadFiles(files: FormData, ticket: Tickets) {
    this._sending = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Ticket/UploadFiles', files, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: ValidationResultsModel) => {
      this._Success = results.valid;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._sending = false;
      },
      () => {
        this._sending = false;
        this.GetMessages(ticket);
      });
  }

  SendReply(ticket: Tickets) {
    this._sending = true; 
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Ticket/SendReply', ticket, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }

    ).subscribe((results: ValidationResultsModel) => {
      
    },
      (error) => {
        console.log(error)
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._sending = false;
      },
      () => {

        this.UploadFiles(this._files, ticket);
    //    this._sending = false;
      });
  }

  DownloadSelectedAttachment(ticket: Tickets) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Ticket/DownloadSelectedDocs', ticket, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }

    ).subscribe((results: Attachment[]) => {

      results.forEach((el) => {
        let _filename = el.name;

        let _filesource = '../../Support/assets/Docs/' + _filename
        

        var a = document.createElement('a');
        a.target = '_blank';
        a.setAttribute('type', 'hidden');
        a.href = _filesource
        a.download = _filename;
        document.body.appendChild(a);
        a.click();
        a.remove();
      })

    },
      (error) => {
        console.log(error)
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._busy = false;
      },
      () => {
        this._busy = false;
      });
  }

  updateTicket(data: Tickets) {
    this._busy = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Ticket/UpdateTicket', data, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }

    ).subscribe((results: ValidationResultsModel) => {
      this._Success = results.valid;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._sending = false;
      },
      () => {

        this._busy = false;
        this.GetAllTickets();
      });
  }

  SubmitNewTicket(ticket: Tickets) {
    this._sending = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Ticket/SubmitNewTicket', ticket, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Content-Type": "application/json",
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }

    ).subscribe((results: ValidationResultsModel) => {

    },
      (error) => {
        console.log(error)
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._sending = false;
      },
      () => {

        this.UploadNewFiles(this._files, ticket);
       // this._sending = false;
      });
  }

  UploadNewFiles(files: FormData, ticket: Tickets) {
    this._sending = true;
    //let token = localStorage.getItem('token');
    this._http.post(this._baseUrl + 'api/Ticket/UploadNewTicketFiles', files, {
      headers: new HttpHeaders({
        //"Authorization": "Bearer " + token,
        "Cache-Control": "no-store",
        "Pragma": "no-cache"

      })
    }
    ).subscribe((results: ValidationResultsModel) => {
      this._Success = results.valid;
    },
      (error) => {
        console.log(error);
        this.error = true;
        this.errorMessage = "ERROR " + error.status + " : " + error.message;
        this._sending = false;
      },
      () => {
        this._sending = false;
      });
  }

  FilterTicket(name: string, status: string) {

    if (status == '1') {
      let temp: Tickets[] = [];
      this._newTickets.forEach((e) => {
        if (e.name == name) {
          temp.push(e);
        }
      });

      this._newTickets = [];

      temp.forEach((el) => {
        this.newTickets.push(el);
      })
    }
    if (status == '2') {
      let temp: Tickets[] = [];
       this._completedTickets.forEach((e) => {
        if (e.name == name) {
          temp.push(e);
        }
      });

      this._completedTickets = [];

      temp.forEach((el) => {
        this._completedTickets.push(el);
      })
    }
    if (status == '3') {
      let temp: Tickets[] = [];
      this._processTickets.forEach((e) => {
        if (e.name == name) {
          temp.push(e);
        }
      });

      this._processTickets = [];

      temp.forEach((el) => {
        this._processTickets.push(el);
      })
    }
    if (status == '4') {
      let temp: Tickets[] = [];
       this._canceledTickets.forEach((e) => {
        if (e.name == name) {
          temp.push(e);
        }
      });

      this._canceledTickets = [];

      temp.forEach((el) => {
        this._canceledTickets.push(el);
      })
    }

  }

  ShowNotification() {

    if (this._router.url == '/done') {
      let todayDate: Date = new Date();
      let todayDateStr = this.datepipe.transform(todayDate, "yyyy/MM/dd");
      let count: number = 0;
      this._completedTickets.forEach((e) => {
        let ticketDate: Date = new Date(e.createDate);
        let ticketDateStr = this.datepipe.transform(ticketDate, "yyyy/MM/dd");
        if (ticketDateStr == todayDateStr) {
          count++;
        }

      });
      this._toastService.success("You have completed " + count + " ticket/s today.", "COMPLETED TICKETS");
    }
    if (this._router.url == '/progress') {
      this._toastService.warning("You have " + this._processTickets.length + " ticket/s that still needs to be completed.", "IN PROCCESS TICKETS");
    }

    if (this._router.url == '/request') {
      this._toastService.error("You have " + this._newTickets.length + " ticket/s that still needs attention.", "REQUESTED TICKETS");
    }

    if (this._router.url == '/canceled') {
      let todayDate: Date = new Date();
      let todayDateStr = this.datepipe.transform(todayDate, "yyyy/MM/dd");
      let count: number = 0;
      this._canceledTickets.forEach((e) => {
        let ticketDate: Date = new Date(e.createDate);
        let ticketDateStr = this.datepipe.transform(ticketDate, "yyyy/MM/dd");
        if (ticketDateStr == todayDateStr) {
          count++;
        }

      });
      this._toastService.info("You have " + count + " canceled ticket/s today.", "CANCELED TICKETS");
    }
  }
}
