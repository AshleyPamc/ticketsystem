export class Password {
    public username: string;
    public oldPassword: string;
    public newPassword: string;
}
