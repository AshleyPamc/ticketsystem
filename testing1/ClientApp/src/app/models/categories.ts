export class Categories {
  public  id:string;
  public desc: string;
  public sla: boolean;
  public username: string;
  public client: string;
  public clientId: string;
}
